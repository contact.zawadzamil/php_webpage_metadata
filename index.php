<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Metadata | PHP | Zawad</title>
</head>
<body>
<?php
require 'vendor/autoload.php';
$web = new spekulatius\phpscraper;
if(isset($_POST['url'])){
    $web->go($_POST['url']);

// Returns "Metadata"

    $result = array(
        "siteTitle"=>$web->title,
        "viewpoint" => $web->viewport,
        "canonical_url" =>  $web->canonical,
        "contentType" =>  $web->contentType,
        "csrfToken" => $web->csrfToken,
        "author" => $web->author,
        "description" =>$web->description,
        "image" => $web->imagesWithDetails,
        "keywords" =>  $web->keywords,
        "headings" =>  $web->headings,
        "contentKeywords" =>  $web->contentKeywords,
        "links" =>  $web->links,


    );


}
?>

<h1 class="display-1 text-center ">
   PHP Web Metadata Finder
</h1>
<div class=" container mt-4" >
    <form action="" method="POST">
        <div class="input-group">
            <label for="input" class=" mt-1" >Insert URL: </label> <br>
            <input type="text" id="input" name="url" class="form-control ps-4 ms-4"  placeholder="ex: https://github.com/">
            <button class="btn btn-success"> SUBMIT

            </button>
        </div>


    </form>




</div>

<?php if (isset($_POST['url'])) : ?>
    <div class="container mt-4">
        <h3 class="text-center">Result</h3>
       <table class="table table-dark">
           <thead class="text-center">
           <tr>
               <th>
                   Field
               </th>
               <th>
                   Value
               </th>
           </tr>

           </thead>
           <tbody class="text-center">
           <tr>
               <td>
                   Title
               </td>
               <td>
                   <?php echo $result['siteTitle'];?>
               </td>
           </tr>
<!--           Viewpoint-->
           <tr>
               <td>
                   Viewpoint
               </td>
               <td>
                   <?php foreach ($result['viewpoint'] as $item){

                       echo $item.PHP_EOL;
                   } ?>
               </td>
           </tr>
<!--           canonical_url-->
           <tr>
               <td>Canonical Url</td>
               <td>
                   <?php echo $result['canonical_url'];?>
               </td>
           </tr>
<!--           contentType-->
           <tr>
               <td class="text-uppercase">contentType </td>
               <td>
                   <?php echo $result['canonical_url'];?>
               </td>
           </tr>

           <!--           csrfToken-->
           <tr>
               <td class="text-uppercase">csrf Token </td>
               <td>
                   <?php echo empty($result['csrfToken'])?'NOT Found':$result['csrfToken'];?>
               </td>
           </tr>

           <!--           author-->
           <tr>
               <td class="text-uppercase">author </td>
               <td>
                   <?php echo empty($result['author'])?'NOT Found':$result['author'];?>
               </td>
           </tr>

           <!--           description-->
           <tr>
               <td class="text-uppercase">description </td>
               <td>
                   <?php echo empty($result['description'])?'NOT Found':$result['description'];?>
               </td>
           </tr>

 <!--           image-->
           <tr>
               <td class="text-uppercase">images </td>
               <td>
                   <?php foreach ($result['image'] as $item):

                   ?>
        <img src="<?php echo $item['url'] ?>" height="100" width="200" alt="">

                   <?php endforeach; ?>
               </td>
           </tr>

           <!--           keywords-->
           <tr>
               <td class="text-uppercase">keywords </td>
               <td>
                   <?php echo empty($result['description'])?'NOT Found':$result['description'];?>
               </td>
           </tr>

           </tbody>
       </table>



    </div>

<?php endif; ?>


<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
-->
</body>
</html>












