# Aide

## Bugs et autres problèmes

Si vous remarquez des bugs ou des problèmes de nature similaire, veuillez envoyer un message à l'adresse suivante [problème sur GitHub](https://github.com/spekulatius/PHPScraper/issues). N'oubliez pas d'inclure les informations détaillées permettant de reproduire le problème.

## Support commercial

L'assistance pour les applications commerciales, les licences commerciales, le développement personnalisé, etc. [Parlons-en ensemble](https://peterthaleikis.com/contact).
